# Twitter_project

Imagen base: Tensorflow oficial ()

Items agregados:
* Jupyter lab 
* Black linter  
	* Node/npm para instalar el linter (https://stackoverflow.com/questions/36399848/install-node-in-dockerfile)
* Working directory: '/home/jovyan/work'
	* Vinculado en el compose con la carpeta: 'work'

## Items del repositorio

* ./
  * Carpeta que se vincula con la carpeta notebooks de jupyter en el docker compose
* ./Jupyterlab
  * Dockerfile
  * Requeriments de la imagen base
* ./docker-compose.yml
  * Docker compose, muy simple
  * Build con la imagen que está en la carpeta jupyterlab
  * Puerto compartido: 28888 (Relacionado al puerto 8888 de jupyterlab)
    * Para ingresar: http://0.0.0.0:28888/lab
* config.ini
  * Configuración necesaria para poder bajar tweets
  * 4 valores a configurar:
    * ACCESS_TOKEN=<access_token>
    * ACCESS_SECRET=<access_secret>
    * API_Key=<api_key>
    * API_Secret=<api_secret>
  * Info sensible, mantener fuera del versionado
  * Configurable desde: https://developer.twitter.com/en
  * Instrucciones para activar la cuenta: https://developer.twitter.com/en/docs/twitter-api/getting-started/getting-access-to-the-twitter-api


## Instrucciones

Builder imagen embebida en el compose

	docker-compose build 

Levantar el docker compose

	docker-compose up -d --no-deps

Para ingresar, en el navegador: http://0.0.0.0:28888/lab


## Requerimientos

* Docker
* Docker compose

## Notebooks

Hay 3 notebooks, básicos para comenzar:

* 00.scraping
  * Usando [Tweepy](https://www.tweepy.org/), y hashtags se bajando los tweets como stream
  * No hay limites ajustados, solo una función que procesa cada mensaje que llega.
  * Por defecto van a un archivo txt en la carpeta: `.\data\`
* 01.01.preproc
  * Flatten de los tweets
  * Filtrado
  * Preprocesamiento básico
  * Por defecto crea un archivo csv por cada archivo txt
* 01.02.metadata
  * Extrae metadata muy simple del dataframe guardado en el csv

